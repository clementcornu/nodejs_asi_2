const express = require('express');
const app = express();
const http = require('http');
const chatserver = http.createServer(app);
const userserver = http.createServer(app);
const io = require("socket.io");
const ioServer = io(chatserver);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.get('localhost:8082/users',(req, res) => {
    res.status(200).json(user);
  });

ioServer.on('connection', function(socket){
    console.log('a user is connected');
    socket.on('disconnect', function (){
        console.log('a user is disconnected');
    })
    socket.on('chat message', function (msg){
        console.log('message recu : ' + msg);
        ioServer.emit('chat message', msg);
    })

})

chatserver.listen(3000, () => {
  console.log('Serveur chat connecté sur le port 3000');
});

app.listen(8082,() => {
    console.log('Serveur user connecté sur le port 8082');
})